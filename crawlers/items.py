# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.loader.processors import TakeFirst

class PttItem(scrapy.Item):

    title = scrapy.Field(output_processor=TakeFirst())
    author = scrapy.Field(output_processor=TakeFirst())
    nickname = scrapy.Field(output_processor=TakeFirst())
    date_ts = scrapy.Field(output_processor=TakeFirst())
    board_name = scrapy.Field(output_processor=TakeFirst())
    score = scrapy.Field(output_processor=TakeFirst())
    category = scrapy.Field(output_processor=TakeFirst())
    if_reply = scrapy.Field(output_processor=TakeFirst())
    content = scrapy.Field(output_processor=TakeFirst())
    ip_address = scrapy.Field(output_processor=TakeFirst())
    comments = scrapy.Field(output_processor=TakeFirst())
    url = scrapy.Field(output_processor=TakeFirst())
