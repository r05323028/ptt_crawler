# -*- coding: utf-8 -*-
import re
from datetime import datetime
import scrapy
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.http import Request, FormRequest
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst
from scrapy.exceptions import DropItem
from crawlers.items import PttItem

def split_author_nickname(author):
    author = author.split(' ')
    return author

def date_ts_parser(date_ts):
    date_ts = datetime.strptime(date_ts,'%a %b %d %H:%M:%S %Y')
    return date_ts

def content_processor(content):
    content = list(map(lambda x: re.sub(r'[\n\xa0\u3000]','',x),content))
    content = list(filter(lambda x: len(x) > 0,content))

    reg_filter = lambda x: re.sub('[ ]{2,}', '', x)
    content = list(map(reg_filter, content))

    content = ''.join(content)

    return content

class PttSpider(CrawlSpider):
    name = 'ptt'
    allowed_domains = ['www.ptt.cc']
    rules = (
        Rule(LxmlLinkExtractor(
            allow = (r'.*'),
            restrict_xpaths = '//div[@class="action-bar"]/div[@class="btn-group btn-group-paging"]'
        )),
        Rule(LxmlLinkExtractor(
            allow=(r'.*'),
            restrict_xpaths= '//div[@id="main-container"]//div[@class="title"]'
        ),
        callback = "parse_post")
    )

    def __init__(self,board_name = 'Gossiping', **kwargs):
        self.board_name = board_name
        self.start_urls = ['https://www.ptt.cc/bbs/%s/index.html'%self.board_name]
        super(PttSpider,self).__init__(**kwargs)


    def parse_start_url(self, response):
        if response.xpath('/html/body/div[1]/div/@class').extract_first() == 'over18-notice':
            yield FormRequest.from_response(
                response,
                formdata = {'yes':'yes'}
                )
        else:
            pass

    def parse_post(self,response):
        try:
            item = ItemLoader(item = PttItem(), response = response)
            item.add_xpath('title','//*[@id="main-content"]/div[3]/span[2]/text()')
            item.add_xpath('if_reply', '//*[@id="main-content"]/div[3]/span[2]/text()', MapCompose(lambda x: 1 if 'Re:' in x else 0))
            item.add_xpath('category','//*[@id="main-content"]/div[3]/span[2]/text()', re = r'(?<=\[).*?(?=\])')
            item.add_xpath('author','//*[@id="main-content"]/div[1]/span[2]/text()',MapCompose(lambda x: x.split(' ')))
            item.add_xpath('nickname','//*[@id="main-content"]/div[1]/span[2]/text()', re = r'(?<=\().*?(?=\))')
            item.add_xpath('date_ts','//*[@id="main-content"]/div[4]/span[2]/text()',TakeFirst(), date_ts_parser)
            item.add_xpath('content','//*[@id="main-content"]/text()',content_processor)
            item.add_value('url',response.url)

            try:
                if response.xpath('//*[@id="main-content"]/text()[2]').re_first(r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+') != None:
                    ip_address = response.xpath('//*[@id="main-content"]/text()[2]').re_first(r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
                elif next(filter(lambda x: '發信站' in x, response.xpath('//span[@class="f2"]/text()').extract())) != None:
                    ip_address = re.search(r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+', next(filter(lambda x: '發信站' in x, response.xpath('//span[@class="f2"]/text()').extract()))).group()
                else:
                    ip_address = None
            except AttributeError:
                ip_address = None

            item.add_value('ip_address',ip_address)
            item.add_value('board_name',self.board_name)
            score = 0

            for i in range(1,len(response.xpath('//div[@id="main-container"]/div[@id="main-content"]//div[@class="push"]'))+1):
                comment = dict()
                if response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[1]/text()'%i).extract_first() == '推 ':
                    comment['comment_score'] = 1
                elif response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[1]/text()'%i).extract_first() == '噓 ':
                    comment['comment_score'] = -1
                else:
                    comment['comment_score'] = 0

                score += comment['comment_score']
                comment['comment_id'] = response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[2]/text()'%i).re_first(r'.*[^ ]')
                comment['comment_content'] = response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[3]/text()'%i).extract_first()[2:]
                comment['comment_ip'] = response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[4]/text()'%i).re_first(r'[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')
                comment['date'] = response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[4]/text()'%i).re_first(r'[0-9]{2}/[0-9]{2}')
                comment['time'] = response.xpath('//div[@id="main-container"]/div[@id="main-content"]/div[@class="push"][%s]/span[4]/text()'%i).re_first(r'[0-9]{2}:[0-9]{2}')
                item.add_value('comments',comment)

            item.add_value('score',score)

            return item.load_item()

        except ValueError:
            raise DropItem("Invalid format.")