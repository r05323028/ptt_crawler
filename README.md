# PTT Crawler

### **Requirements**
---
- python >= 3.5
- scrapy >= 1.5

### **Usage**
---
You need to change the directory to the project folder,

```
cd ptt_crawler
```

and use the scrapy command line tool. 

*board_name* can let you determine what board you want to crawl.

It is recommend that setting a CLOSESPIDER_ITEMCOUNT
 to control the number of posts crawled.

```    
scrapy crawl ptt -a board_name=Gossiping -s CLOSESPIDER_ITEMCOUNT=100 -o output.json
```